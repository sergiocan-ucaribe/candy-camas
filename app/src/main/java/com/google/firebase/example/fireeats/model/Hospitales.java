package com.google.firebase.example.fireeats.model;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.sql.Array;

/**
 * Restaurant POJO.
 */
enum Estatus {
    Emergencia_sanitaria, 
    Orden_de_llegada, 
    Necesidad
};
@IgnoreExtraProperties
public class Hospitales {

    private int id;
    private Estatus status;
    private String hospital_name;
    private String camas_hospitalizacion[];
    private String camas_terapia[];
    private String pacientes[];


    public Hospitales() {}

    public Hospitales(String hospital_name, int id, Estatus status, String camas_hospitalizacion[], String camas_terapia[], String pacientes[])
    {
        this.hospital_name = hospital_name;
        this.id= id;
        this.status = status;
    }
}