package com.google.firebase.example.fireeats.model;

public class Pacientes {

    private int id;
    private SOFA sofa;
    private float prioridad;
    private int age;
    private String name;
    private boolean personal_salud;
    private boolean requiere_respirador;

    public Pacientes() {}

    public Pacientes(int id, float prioridad, int age, String name, boolean personal_salud, boolean requiere_respirador)
    {
        this.id=id;
        this.name=name;
        this.age=age;
        this.personal_salud=personal_salud;
        this.prioridad=prioridad;
        this.requiere_respirador=requiere_respirador;

    }



}
