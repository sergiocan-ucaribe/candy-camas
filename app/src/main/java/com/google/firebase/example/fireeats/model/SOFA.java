package com.google.firebase.example.fireeats.model;

public class SOFA {
    private float creatinina;
    private float diuresis;
    private float bilirrubina;
    private float pO2fIO2;
    private float presion_arterial;
    private float plaquetas;
    private int glasgowCS;
    private int dA_NA_status;

    public SOFA() {}

    public SOFA(float creatinina, float diuresis, float bilirrubina, float pO2fIO2, float presion_arterial, float plaquetas, int glasgowCS, int dA_NA_status)
    {
        this.creatinina=creatinina;
        this.diuresis=diuresis;
        this.bilirrubina=bilirrubina;
        this.pO2fIO2=pO2fIO2;
        this.presion_arterial=presion_arterial;
        this.plaquetas=plaquetas;
        this.dA_NA_status=dA_NA_status;
        this.glasgowCS=glasgowCS;

    }
}
