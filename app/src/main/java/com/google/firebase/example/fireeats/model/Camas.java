package com.google.firebase.example.fireeats.model;

public class Camas {
  
  private String id;
  private Pacientes paciente;
  private boolean has_respirator;

  public Camas() {}

  public Camas(String id, Pacientes paciente, boolean has_respirator) 
  {
    this.id = id;
    this.paciente = paciente;
    this.has_respirator = has_respirator;
  }
}
